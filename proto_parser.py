"""Module for parsing a JSON representation of a protobuf file.

Optionally it's possible to parse a CLI help output (if implemented with
click).
"""

from collections import OrderedDict
from json import loads
from re import sub
from subprocess import check_output

CLI_NAME = ''

ALL_LANGUAGES = [
    'cpp', 'cs', 'go', 'java', 'javascript', 'php', 'python', 'ruby',
]
LANGUAGES = []

JSON = {}
SCALAR_VALUE_TYPES = {}

REPEATED_TRANSLATIONS = {
    'cpp': 'array',
    'cs': 'array',
    'go': 'array',
    'java': 'array',
    'javascript': 'array',
    'php': 'array',
    'python': 'list',
    'ruby': 'array',
}


def _get_camel_case(string):
    """Return the given string in camelCase."""
    if '_' in string:
        string = ''.join([word.capitalize() for word in string.split('_')])
    return string[0].lower() + string[1:]


def _get_camel_case_setter(proto_param):
    """Get the camelCase setter method for the given proto parameter."""
    return _get_camel_case('set_' + proto_param)


def _get_go_field_name(proto_param):
    """Format the given proto parameter as a go parameter."""
    if proto_param.startswith('_'):
        proto_param = sub('^_', 'X', proto_param)
    return _get_pascal_case(proto_param)


def _get_pascal_case(string):
    """Return the given string in PascalCase."""
    if '_' in string:
        return ''.join([word.capitalize() for word in string.split('_')])
    return string[0].upper() + string[1:]


def _get_scalar_value_types():
    """Get the scalar value types from the JSON file."""
    proto_types = {}
    for scalar_value_type in JSON['scalarValueTypes']:
        translations = {}
        for lang in LANGUAGES:
            if lang == 'javascript':
                translations[lang] = 'number'
                if scalar_value_type['protoType'].endswith('64'):
                    translations[lang] = 'Long'
                if scalar_value_type['protoType'] == 'string':
                    translations[lang] = 'string'
                if scalar_value_type['protoType'] == 'bool':
                    translations[lang] = 'boolean'
                if scalar_value_type['protoType'] == 'bytes':
                    translations[lang] = 'Uint8Array'
                continue
            translations[lang] = scalar_value_type['{}Type'.format(lang)]
        proto_types[scalar_value_type['protoType']] = translations
    return proto_types


def _parse_field_type(param_type, label):
    """Translate the given parameter type for all requested languages."""
    type_dict = {'proto': param_type}
    if label == 'repeated':
        type_dict = {'proto': f'{label} {param_type}'}
    for lang in LANGUAGES:
        if param_type not in SCALAR_VALUE_TYPES:
            type_dict[lang] = param_type
        else:
            type_dict[lang] = SCALAR_VALUE_TYPES[param_type][lang]
        if label == 'repeated':
            type_dict[lang] = '{} {}'.format(REPEATED_TRANSLATIONS[lang],
                                             type_dict[lang])
    return type_dict


def _parse_description(description):
    """Parse the description of an RPC method."""
    if description is None:
        return None
    return description.replace('\n', ' ').replace('\r', '')


def _parse_field_name(name):
    """Parse the proto field name.

    Parse the name of a proto message field and return translations for all
    requested languages.

    The 'proto' key will contain the field name as found in the proto file.
    All languages that don't differ from its definition keep the same name as
    in proto.
    """
    camel_case_name = _get_camel_case(name)
    pascal_case_name = _get_pascal_case(name)
    camel_case_setter = _get_camel_case_setter(name)
    lower_snake_case_setter = f'set_{name}'
    name = {
        'cpp': name,
        'cpp_set': lower_snake_case_setter,
        'cs': pascal_case_name,
        'go': _get_go_field_name(name),
        'java': camel_case_name,
        'java_set': camel_case_setter,
        'javascript': camel_case_name,
        'javascript_set': camel_case_setter,
        'php': camel_case_setter,
        'php_set': camel_case_setter,
        'proto': name,
        'python': name,
        'ruby': name,
    }
    return name


def _parse_message_fields(fields):
    """Parse the fields of a proto message and return them as a list."""
    params = []
    for field in fields:
        name = _parse_field_name(field['name'])
        description = _parse_description(field['description'])
        param_type = _parse_field_type(field['longType'], field['label'])
        parsed_param = {
            'name': name,
            'description': description,
            'param_type': param_type,
        }
        if field['longType'] not in SCALAR_VALUE_TYPES:
            parsed_param['link'] = field['longType'].lower().replace('.', '-')
        params.append(parsed_param)
    return params


def _parse_cli_help(cli_cmd):
    """Parse the help output from a CLI (implemented with click) command."""
    help_output = check_output([CLI_NAME, cli_cmd, '--help']) \
        .decode('utf-8').split('\n')
    cli_info = {
        'usage': '',
        'options': [],
        'description': '',
    }
    section_headers = ['Usage:', 'Options:']
    while len(help_output) > 0:
        line = help_output.pop(0).strip()
        if line == '':
            continue
        if section_headers[0] in line:
            cli_info['usage'] = line.replace(section_headers[0], '').strip()
        elif section_headers[1] in line:
            while len(help_output) > 0:
                line = help_output.pop(0).strip()
                if line == '':
                    continue
                if '--help' not in line and line.startswith('--'):
                    opt = line.split()[0]
                    if line.split()[1].isupper():
                        opt = ' '.join(line.split()[:2])
                    cli_info['options'].append(opt)
        else:
            cli_info['description'] = line
    if not cli_info['options']:
        cli_info['usage'] = cli_info['usage'].replace(' [OPTIONS]', '')
    return cli_info


def _parse_messages():
    """Parse the different messages found within the JSON file."""
    messages = {}
    for message in JSON['files'][0]['messages']:
        name = message['name']
        params = _parse_message_fields(message['fields'])
        messages[name] = {
            'name': name,
            'params': params,
            'link': name.lower(),
        }
    return messages


def _parse_methods(service, messages):
    """Parse the different methods of the given service."""
    methods = []
    for method in service['methods']:
        method['nameCamelCase'] = _get_camel_case(method['name'])
        method['service'] = service['name']
        method['description'] = _parse_description(method['description'])
        method['requestMessage'] = messages[method['requestType']]
        method['responseMessage'] = messages[method['responseType']]
        if CLI_NAME:
            method['cliName'] = method['name'].lower()
            method['cliInfo'] = _parse_cli_help(method['cliName'])
        methods.append(method)
    return methods


def _get_services():
    """Get the different services found within the JSON file."""
    services = []
    for service in JSON['files'][0]['services']:
        services.append(service)
    return services


def _parse_enum_values(enum):
    """Parse the parameters of the given enum and returns them as a list."""
    params = []
    for param in enum['values']:
        description = _parse_description(param['description'])
        parsed_param = {
            'name': param['name'],
            'description': description,
            'number': param['number'],
        }
        params.append(parsed_param)
    return params


def _parse_enums():
    """Parse the different enums found within the JSON file."""
    enums = {}
    for enum in JSON['files'][0]['enums']:
        name = enum['longName']
        params = _parse_enum_values(enum)
        enums[name] = {
            'name': name,
            'description': enum['description'],
            'params': params,
            'link': name.lower(),
        }
    return enums


def parse_proto_json(proto_json_path, cli_name='', languages=None):
    """Parse a proto JSON representation file into a dictionary.

    The dictionary will contain the services, ordered messages
    and ordered enums of the given protobuf definition.
    """
    if cli_name:
        global CLI_NAME
        CLI_NAME = cli_name
    with open(proto_json_path, 'rb') as file:
        json_file = file.read().decode('utf-8')
    global JSON
    JSON = loads(json_file)
    global LANGUAGES
    LANGUAGES = ALL_LANGUAGES
    if languages:
        if all(lang in ALL_LANGUAGES for lang in languages):
            LANGUAGES = languages
        else:
            wrong = [lang for lang in languages if lang not in ALL_LANGUAGES]
            raise RuntimeError(f'Requested unsupported languages: {wrong}')
    global SCALAR_VALUE_TYPES
    SCALAR_VALUE_TYPES = _get_scalar_value_types()
    enums = _parse_enums()
    services = _get_services()
    messages = _parse_messages()
    for service in services:
        service['methods'] = _parse_methods(service, messages)
    ordered_messages = OrderedDict(sorted(messages.items()))
    ordered_enums = OrderedDict(sorted(enums.items()))
    return {
        'services': services,
        'messages': ordered_messages,
        'enums': ordered_enums
    }
