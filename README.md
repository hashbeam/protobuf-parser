# Protobuf parser

This repository contains a Python parser for the JSON representation of a
protobuf definition file (obtained by running the
[`protoc-gen-doc`](https://github.com/pseudomuto/protoc-gen-doc) plugin on the
proto file).

Optionally it can parse a CLI help output (currently only if CLI has been
implemented with [`click`](https://github.com/pallets/click)).

Supported languages:
- cpp
- cs
- go
- java
- javascript
- php
- python
- ruby

## Usage

In order to use this module you need to add this project as a git submodule
or directly copy the module content.

### Example usage

Given a `<proto>.json` file:

```python
from proto_parser import parse_proto_json

parsed_proto = parse_proto_json('path/to/proto.json')
```

Optionally you can also set a CLI name or a sub-list of the supported
languages:

```python
from proto_parser import parse_proto_json

parsed_proto = parse_proto_json('path/to/proto.json',
                                cli_name='cli_name',
                                languages=['go', 'python'])
```
